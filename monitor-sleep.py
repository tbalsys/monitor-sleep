#!/usr/bin/python3
from gi.repository import GLib, Gio


class DisplayConfig:
    def __init__(self):
        self.session_bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
        self.saved_scale = 1.0

    def display_config(
        self,
        bus_name = 'org.gnome.Mutter.DisplayConfig',
        object_path = '/org/gnome/Mutter/DisplayConfig',
        interface_name = 'org.gnome.Mutter.DisplayConfig',
        method_name = None,
        parameters = None,
        reply_type = None,
        flags = Gio.DBusCallFlags.NONE,
        timeout_msec = -1,
        cancellable = None
    ):
        return self.session_bus.call_sync(
            bus_name, object_path, interface_name,
            method_name, parameters, reply_type,
            flags, timeout_msec, cancellable)

    def get_current_scale(self):
        return self.display_config(method_name = 'GetCurrentState')[2][0][2]

    def set_scale(self, scale):
        current_state = self.display_config(method_name = 'GetCurrentState')

        serial, connected_monitors, logical_monitors, _ = current_state
        
        # Multiple monitors are more complicated. For now, since I only use one monitor
        # in Ubuntu, everything is hard-coded so that only info about the first monitor
        # is used, and only it will be connected after running the script.
        #
        # If someday updating this script: a logical monitor may appear on mutiple
        # connected monitors due to mirroring.
        connector = connected_monitors[0][0][0]
        current_mode = None
        # ApplyMonitorsConfig() needs (connector name, mode ID) for each connected
        # monitor of a logical monitor, but GetCurrentState() only returns the
        # connector name for each connected monitor of a logical monitor. So iterate
        # through the globally connected monitors to find the mode ID.
        for mode in connected_monitors[0][1]:
            if mode[6].get("is-current", False):
                current_mode = mode[0]

        updated_connected_monitors = [[connector, current_mode, {}]]
        
        x, y, _, transform, primary, monitors, props = logical_monitors[0]
        
        monitor_config = [[x, y, scale, transform, primary, updated_connected_monitors]]
        
        # Change the 1 to a 2 if you want a "Revert Settings / Keep Changes" dialog
        self.display_config(
            method_name = 'ApplyMonitorsConfig',
            parameters = GLib.Variant('(uua(iiduba(ssa{sv}))a{sv})', (serial, 1, monitor_config, {})))


    def on_prepare_for_sleep(self, conn, sender, obj, interface, signal, parameters, data):
        if parameters[0]:
            print('Going to sleep')
            self.saved_scale = self.get_current_scale()
        else:
            print('Woke up from sleep')
            # Set bus after wake up to avoid error:
            # gi.repository.GLib.GError: g-dbus-error-quark: GDBus.Error:org.freedesktop.DBus.Error.AccessDenied: The requested configuration is based on stale information
            self.session_bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
            self.set_scale(self.saved_scale)


if __name__ == '__main__':
    dc = DisplayConfig()

    system_bus = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)
    system_bus.signal_subscribe(
        'org.freedesktop.login1',
        'org.freedesktop.login1.Manager',
        'PrepareForSleep',
        '/org/freedesktop/login1',
        None,
        Gio.DBusSignalFlags.NONE,
        dc.on_prepare_for_sleep,
        None)
    GLib.MainLoop().run()


