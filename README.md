Prevent Linux GNOME desktop from loosing scale factor after sleep.

```
sudo cp monitor-sleep.py /usr/local/bin
mkdir -p ~/.config/systemd/user/
cp monitor-sleep.service ~/.config/systemd/user/
systemctl --user daemon-reload
systemctl --user enable --now monitor-sleep.service
```
